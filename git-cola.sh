#!/usr/bin/env bash

GOT_COLA_SETTINGS=~/.config/git-cola/settings

# Save find result to F_ARRAY
readarray -t F_ARRAY <<< $(sed -e 's/^\"//' -e 's/"$//' <<< "$(jq '.recent[] | .path' $GOT_COLA_SETTINGS)")

# Associative array for storing paths
declare -A PATHS

# Add elements to PATHS array
get_paths() {

  if [[ ! -z ${F_ARRAY[@]} ]]; then
    for i in "${!F_ARRAY[@]}"
    do
      path=${F_ARRAY[$i]}
      # file=$(basename "${F_ARRAY[$i]}")
      PATHS+=(["$path"]="$path")
    done
  fi


}

# List for rofi
gen_list(){
  for i in "${!PATHS[@]}"
  do
    echo "$i"
  done
}

main() {
  get_paths
  path=$( (gen_list) | rofi -dmenu -i -matching fuzzy -no-custom -location 0 -p "repo > " )

  if [ -n "$path" ]; then
    git-cola -r "${PATHS[$path]}"
  fi
}

main

exit 0
